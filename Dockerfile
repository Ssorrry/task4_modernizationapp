# Базовый образ
FROM python:3.9-slim

# Установка зависимостей
RUN pip install fastapi uvicorn redis pytest

# Копирование исходного кода в образ
COPY Task4_ModernizationApp /app
COPY mytest.py /app 
WORKDIR /app

# Открытие порта
EXPOSE 8080

# Команда для запуска сервера
CMD ["uvicorn", "task4_modernizationapp:app", "--host", "0.0.0.0", "--port", "8080"]

