from task4_modernizationapp import binary_search

def test_binary_search():
    # Простой тестовый массив для проверки
    test_array = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
    
    # Тесты для бинарного поиска
    test_cases = [
        # Поиск существующего значения
        (test_array, 5, 2),
        # Поиск значения, которого нет в массиве
        (test_array, 4, -1),
        # Поиск первого элемента массива
        (test_array, 1, 0),
        # Поиск последнего элемента массива
        (test_array, 19, 9),
        # Поиск в пустом массиве
        ([], 5, -1)
    ]

    for arr, search_val, expected_index in test_cases:
        # Проверка результата бинарного поиска
        assert binary_search(arr, search_val) == expected_index

# Запуск теста
if __name__ == "__main__":
    test_binary_search()
    