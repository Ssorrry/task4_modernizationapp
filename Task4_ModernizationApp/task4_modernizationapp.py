"""
Модуль для выполнения бинарного поиска в отсортированном массиве.
"""

import os
import sys
from fastapi import FastAPI
import redis
import random
import pickle

app = FastAPI()
r = redis.Redis(host='redis', port=6379, db=0)

def generate_sorted_array(size):
    """
    Генерирует отсортированный массив случайных чисел.

    :param size: Размер массива.
    :return: Отсортированный массив случайных чисел.
    """
    return sorted([random.randint(1, 1000) for _ in range(size)])

def binary_search(arr, tgt):
    """
    Выполняет бинарный поиск в отсортированном массиве.

    :param arr: Отсортированный массив.
    :param tgt: Искомое значение.
    :return: Индекс искомого значения в массиве или -1, если значение не найдено.
    """
    left, right = 0, len(arr) - 1
    while left <= right:
        mid = (left + right) // 2
        if arr[mid] == tgt:
            return mid
        if arr[mid] < tgt:
            left = mid + 1
        else:
            right = mid - 1
    return -1

def get_or_generate_array():
    array_pickle = r.get("sorted_array")
    if array_pickle:
        array = pickle.loads(array_pickle)
    else:
        array = generate_sorted_array(100)
        r.set("sorted_array", pickle.dumps(array))
    return array

@app.get("/search/{search_value}")
def search_number(search_value: int):
    array = get_or_generate_array()
    index = binary_search(array, search_value)
    return {"index": index}
